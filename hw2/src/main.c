#include "../lib/macro-logger/macrologger.h"
#include "../lib/tgc/tgc.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
static tgc_t gc;
#ifdef DEBUG_MODE
#define DEBUG
#endif
// Other stuff
typedef int bool;
#define true 1
#define false 0
// Make an Nary Tree Structure
int ROOT_NODE = 0;
int EXPRESSION_NODE = 1;
int VALUE_NODE = 2;

typedef struct node {
  int val;
  int node_type;
  bool is_terminator;
  int (*expr)(int, int);
  struct node *sibling;
  struct node *child;
} node_t;
typedef int (*FncPtr)(int, int);
typedef int (*EvalExprPtr)(node_t *node);

// Node stuff
int default_expression(int a, int b) {
  // printf("SAW: %d %d\n", a, b);
  return 0;
}
int identity_expression(int a, int b) {
  LOG_DEBUG("You fucked up\n");
  return b;
}
int add_expression(int a, int b) {
  default_expression(a, b);
  int ret = 0;
  ret = a + b;
  // LOG_DEBUG("returning : %d + %d = %d\n", a, b, ret);
  return ret;
}
int subtract_expression(int a, int b) {
  default_expression(a, b);
  return a - b;
}
int multiply_expression(int a, int b) {
  default_expression(a, b);
  int ret = 0;
  ret = a * b;
  // LOG_DEBUG("returning : %d * %d = %d\n", a, b, ret);
  return ret;
}
int divide_expression(int a, int b) {
  default_expression(a, b);
  if (b == 0) {
    printf("PID %d: ERROR: division by zero is not allowed; exiting\n",
           getpid());
    return 0;
  }
  return a / b;
}
int eq_expression(int a, int b) { return a == b; }
int lt_expression(int a, int b) { return a < b; }
int gt_expression(int a, int b) { return a > b; }
int lte_expression(int a, int b) { return a <= b; }
int gte_expression(int a, int b) { return a >= b; }
int neq_expression(int a, int b) { return a != b; }
int mod_expression(int a, int b) { return a % b; }
int and_expression(int a, int b) { return a & b; }
int or_expression(int a, int b) { return a | b; }
int xor_expression(int a, int b) { return a ^ b; }

FncPtr expression_factory(char n) {
  // printf("Got parameter %c\n", n);
  FncPtr functionPtr;
  if (n == '+') {
    functionPtr = &add_expression;
  } else if (n == '-') {
    functionPtr = &subtract_expression;
  } else if (n == '*') {
    functionPtr = &multiply_expression;
  } else if (n == '/') {
    functionPtr = &divide_expression;
  } else if (n == '=') {
    functionPtr = &eq_expression;
  } else if (n == '>') {
    functionPtr = &gt_expression;
  } else if (n == '<') {
    functionPtr = &lt_expression;
  } else if (n == '!') {
    functionPtr = &neq_expression;
  } else if (n == ']') {
    functionPtr = &gte_expression;
  } else if (n == '[') {
    functionPtr = &lte_expression;
  } else if (n == '%') {
    functionPtr = &mod_expression;
  } else if (n == '&') {
    functionPtr = &and_expression;
  } else if (n == '|') {
    functionPtr = &or_expression;
  } else if (n == '^') {
    functionPtr = &xor_expression;
  } else {
    functionPtr = &default_expression;
  }
  return functionPtr;
}
char *string_default() {
  char *report = tgc_alloc(&gc, 64 * sizeof(char));
  memset(report, '\0', 64);
  // report [0] = '\0';
  return report;
}
char *string_from(char *given) {
  char *report = string_default();
  strcpy(report, given);
  return report;
}

char *string_size(unsigned int size) {
  char *report = tgc_alloc(&gc, size);
  return report;
}
node_t *node_expression_from(int (*expr)(int, int), char tok) {
  node_t *report = tgc_alloc(&gc, sizeof(node_t));
  report->expr = expr;
  // report->node_type = tgc_alloc(&gc,sizeof(int));
  report->node_type = EXPRESSION_NODE;
  report->sibling = NULL;
  report->child = NULL;
  report->val = (int)tok;
  report->is_terminator = false;
  return report;
}
node_t *node_value_from(int val) {
  node_t *report = tgc_alloc(&gc, sizeof(node_t));
  report->val = val;
  report->node_type = VALUE_NODE;
  report->sibling = NULL;
  report->child = NULL;
  report->is_terminator = false;
  report->expr = &identity_expression;
  return report;
}
void make_terminator(node_t *node) {
  node->sibling = NULL;
  node->child = NULL;
  node->is_terminator = true;
}
int operate_on_children(node_t *first_child) { return 0; }
void print_node(node_t *node) {
  // if (node->node_type == VALUE_NODE) {
  //   LOG_DEBUG("(value: %d)", node->val);
  // }
  // LOG_DEBUG("(type: %d) (n addr: %u) (s addr: %u) (c addr:%u) (is t: %d)\n",
  //           (int)node->node_type, (unsigned int)node,
  //           (unsigned int)node->sibling, (unsigned int)node->child,
  //           (int)node->is_terminator);
}
void bfs(node_t *node) {
  if (node == NULL) {
    // LOG_DEBUG("(node was NULL)\n");
    return;
  }
  if (node->node_type == EXPRESSION_NODE) {
    printf(" (%c", (char)node->val);
    bfs(node->child);
    printf(")");
  }
  if (node->node_type == VALUE_NODE) {
    printf(" %d", node->val);
    bfs(node->sibling);
  }
}
int eval_expression_node(node_t *node) {
  if (!node) {
    return 0;
  }

  // LOG_DEBUG("(type: %d) (n addr: %u) (s addr: %u) (c addr:%u) (is t: %d)\n",
  //           (int)node->node_type, (unsigned int)node,
  //           (unsigned int)node->sibling, (unsigned int)node->child,
  //           (int)node->is_terminator);
  node_t *next = node->child->sibling;
  int report = node->child->val;
  while (next) {

    if (next->node_type == VALUE_NODE) {
      report = node->expr(report, next->val);
    } else {
      printf("PID %d: Starting \"%c\" operation\n", getpid(), node->val);
      // TODO: fork the thing right here
      // report = node->expr(report, eval_expression_node(next));
      int dis_pipe[2];
      int rc = pipe(dis_pipe);
      if (rc == -1) {
        LOG_ERROR("pipe() failure");
        return EXIT_FAILURE;
      }
      LOG_DEBUG("Created pipe; p[0] is %d and p[1] is %d\n", dis_pipe[0],
                dis_pipe[1]);

      pid_t pid = fork();
      if (pid == -1) {
        LOG_ERROR("fork() failure");
        return EXIT_FAILURE;
      }
      LOG_DEBUG("FORK WORKED! %d", pid);

      if (pid == 0) {
        LOG_DEBUG("CHILD %d : I'm the child!", pid);
        // Child
        close(dis_pipe[0]);
        dis_pipe[0] = -1;
        // report = node->expr(report, eval_expression_node(next));
        int ret = node->expr(report, eval_expression_node(next));
        printf("PID %d: Processed ", getpid());
        bfs(node);
        printf("; sending \"%d\" on pipe to parent\n", ret);
        write(dis_pipe[1], &ret, sizeof(ret));
        exit(EXIT_SUCCESS);
      } else if (pid > 0) {
        LOG_DEBUG("PARENT %d : I'm the parent!", pid);
        // parent
        close(dis_pipe[1]);
        dis_pipe[1] = -1;

        int ret;
        read(dis_pipe[0], &ret, sizeof(ret));

        int status;
        pid_t child_pid = waitpid(-1, &status, 0);
        if (WIFSIGNALED(status))
        {
          printf("PID %d: child terminated abnormally [child pid %d]\n",getpid(),child_pid);
        } else if (WIFEXITED(status)) /* child proc returned or called exit() */
        {
          if(status != EXIT_SUCCESS){
            printf("PID %d:child terminated with nonzero exit status %d", getpid(), status);
          }
        }
        LOG_DEBUG("PARENT SAW CHILD: %d GOT %d ret", child_pid, ret);
        report = ret;

      } else {
        return EXIT_FAILURE;
      }
    }
    next = next->sibling;
  }
  return report;
}

char *load_file(char *restrict str, int size, FILE *restrict stream) {
  // LOG_DEBUG("Reading:\n{\n %s}\n",str);
  char *temp = string_default();
  char *ret;
  ret = fgets(temp, 64, stream);
  if (ret == 0) {
    // perror("read error, couldn't read from file\n");
    return ret;
  }
  if (temp[strlen(temp) - 1] != '\n') {
    LOG_ERROR("parse error, newline expected\n");
    return 0;
  } else {
    temp[strlen(temp) - 1] = 0;
  }
  strcpy(str, temp);
  // LOG_DEBUG("Done Reading");
  return ret;
}
/*
parse_string: takes string expression and returns the root of an ast
representing that expression.
*/
bool is_number(char c) {
  if ('0' <= c && c <= '9') {
    return true;
  }
  return false;
}
bool is_known_operator(char e) {
  return (e == '+' || e == '*' || e == '-' || e == '/' || e == '>' ||
          e == '<' || e == ']' || e == '[' || e == '=' || e == '!' ||
          e == '&' || e == '^');
}
bool is_alpha(char e) { return (e > 'A' && e < 'z'); }
node_t *parse_string(char *expression) {
  LOG_DEBUG("got %s", expression);
  if (!expression) {
    return NULL;
  }
  node_t *report = NULL;
  int length = strlen(expression);
  enum states { RPAREN, LPAREN, OPERATOR, NUMBER, NUMBER_OR_RPAREN };
  // node_t* e_node_ptr = NULL;
  // node_t* v_node_ptr = NULL;
  int head = 0;
  node_t *stack[64] = {NULL}; // TODO: not this
  enum states expected = RPAREN;
  for (int i = 0; i < length; i++) {

    if (expression[i] == ' ') {
      continue;
    }
    if (expression[i] == ')') {
      // LOG_DEBUG("collpase the stack\n");
    } else if (expression[i] == '(') {
      // LOG_DEBUG("push new expression onto the stack\n");
      expected = OPERATOR;
      // look ahead and make sure the operand is good
      int peek_i = 1;
      while (expression[i + peek_i] == ' ') {
        LOG_DEBUG("%c", expression[i + peek_i]);
        peek_i++;
      }
      if (!is_known_operator(expression[i + peek_i])) {
        char *uknop = string_default();
        int peek_j = 0;
        while (is_alpha(expression[i + peek_i + peek_j])) {
          uknop[peek_j] = expression[i + peek_i + peek_j];
          peek_j++;
        }
        expression[i + peek_i + peek_j] = '\0';
        // LOG_DEBUG(" last was %d %c", peek_j, uknop[peek_j]);
        printf("PID %d: ERROR: unknown \"%s\" operator; exiting", getpid(),
               uknop);
        exit(EXIT_FAILURE);
      }
    }
    if (expression[i] == '+' || expression[i] == '*' || expression[i] == '-' ||
        expression[i] == '/' || expression[i] == '>' || expression[i] == '<' ||
        expression[i] == ']' || expression[i] == '[' || expression[i] == '=' ||
        expression[i] == '!' || expression[i] == '&' || expression[i] == '^') {
      // LOG_DEBUG("Add expression to top node of the stack\n");
      expected = NUMBER_OR_RPAREN;

      if (!report) {
        // If its the first one just set it and contine;
        report = node_expression_from(expression_factory(expression[i]),
                                      expression[i]);
        report->node_type = EXPRESSION_NODE;
        printf("PID %d: Starting \"%c\" operation\n", getpid(), expression[i]);
        // LOG_DEBUG("--> %d %d %c",EXPRESSION_NODE, report->node_type,
        // report->val);
        stack[head] = report;
        head++;
        continue;
      }
      if (!stack[head]) {
        printf("PID %d: Starting \"%c\" operation\n", getpid(), expression[i]);
        stack[head] = node_expression_from(expression_factory(expression[i]),
                                           expression[i]);
      }
      stack[head - 1]->sibling = stack[head];
      head++;
    }
    if (is_number(expression[i])) {
      // LOG_DEBUG("Add value node to top node of the stack\n");
      // TODO: parse numbers longer than 1
      // peek ahead and see if there's a number so we can parse it
      char str_number[64] = {'?'};
      int peek_i = 0;

      while (expression[i + peek_i] && is_number(expression[i + peek_i])) {
        str_number[peek_i] = expression[i + peek_i];
        // LOG_DEBUG("SAW NUMBER : %c\t", expression[i + peek_i]);
        peek_i++;
      }
      // str_number[peek_i + 1] = '\0';
      // LOG_DEBUG("OUR NUMBER IS :::: %c\n", str_number[0]);
      char *p_end = NULL;
      int number = (int)strtol(str_number, &p_end, 10);
      stack[head] = node_value_from(number);
      // LOG_DEBUG("--> GOT NUM: %d FF NUM: %d\t\n", number, peek_i);
      // TODO: Line that makes it just handle 1 number (current expression)
      stack[head] = node_value_from(number);
      if (stack[head - 1]->node_type == EXPRESSION_NODE) {
        stack[head - 1]->child = stack[head];
      } else {
        stack[head - 1]->sibling = stack[head];
      }
      printf("PID %d: My expression is \"%d\" \n", getpid(), number);
      printf("PID %d: Sending \"%d\" on pipe to parent\n", getpid(), number);
      print_node(stack[head - 1]);
      print_node(stack[head]);
      head++;
      i = i + peek_i;
      expected = NUMBER_OR_RPAREN;
    }
  }
  return report;
}
char *read_file(char *filename) {
  LOG_DEBUG("reading file %s", filename);
  char *buffer = string_default();
  char *report = string_default();
  FILE *fp = fopen(filename, "r");
  while (fgets(buffer, sizeof(buffer), fp)) {
    strcat(report, buffer);
    LOG_DEBUG("%s", buffer);
  }
  fclose(fp);
  return report;
}
#ifdef DEBUG
void debug_test_1() {
  node_t *a = node_value_from(0);
  node_t *b = node_value_from(1);
  node_t *c = node_value_from(2);
  node_t *d = node_value_from(3);
  node_t *e = node_value_from(4);
  LOG_DEBUG("NODE A : %d %d\n", a->node_type, a->val);
  LOG_DEBUG("NODE B : %d %d\n", b->node_type, b->val);
  LOG_DEBUG("NODE C : %d %d\n", c->node_type, c->val);
  LOG_DEBUG("NODE D : %d %d\n", d->node_type, d->val);
  LOG_DEBUG("NODE E : %d %d\n", e->node_type, e->val);

  node_t *expr1 = node_expression_from(&add_expression, '+');
  expr1->val = (int)'+';
  LOG_DEBUG("NODE expr1: %d \n", expr1->node_type);
  LOG_DEBUG("(+ A B) => %d\n", expr1->expr(c->val, d->val));

  // Add nodes to expr1 so they can be summed.

  expr1->child = a;
  a->sibling = b;
  b->sibling = c;
  c->sibling = d;
  d->sibling = e;
  make_terminator(e);

  // RUN BFS Algo on root node.
  // bfs(expr1);

  // Make a new set of nodes for the second leg.
  node_t *f = node_value_from(5);
  node_t *g = node_value_from(6);
  node_t *h = node_value_from(7);
  node_t *i = node_value_from(8);
  node_t *j = node_value_from(9);
  LOG_DEBUG("NODE F : %d %d\n", f->node_type, f->val);
  LOG_DEBUG("NODE G : %d %d\n", g->node_type, g->val);
  LOG_DEBUG("NODE H : %d %d\n", h->node_type, h->val);
  LOG_DEBUG("NODE I : %d %d\n", i->node_type, i->val);
  LOG_DEBUG("NODE J : %d %d\n", j->node_type, j->val);

  // Make a new expression to house this new set of nodes
  node_t *expr2 = node_expression_from(&add_expression, '+');
  LOG_DEBUG("NODE expr1: %d %c\n", expr1->node_type, (char)expr1->val);

  // Link this new expression with the last sibling of the first set

  e->sibling = expr2;
  expr2->child = f;
  f->sibling = g;
  g->sibling = h;
  h->sibling = i;
  i->sibling = j;
  make_terminator(j);

  // RUN BFS Algo on root node, this time with more nodes to do.
  // bfs(expr1);

  // Eval Expression node 2
  LOG_DEBUG("EVAL NODE expr2: %d\n", eval_expression_node(expr2));
  LOG_DEBUG("\n\n");
  // Eval Expression node 1
  LOG_DEBUG("EVAL NODE expr1: %d\n", eval_expression_node(expr1));

  LOG_DEBUG("\n\n ----- \n\n");
  LOG_DEBUG("\n\n ----- \n\n");

  // Multiply two numbers together.
  node_t *expr4 = node_expression_from(&multiply_expression, '*');
  node_t *m1 = node_value_from(3);
  node_t *m2 = node_value_from(4);
  node_t *m3 = node_value_from(5);

  expr4->child = m1;
  m1->sibling = m2;
  m2->sibling = m3;
  make_terminator(m3);
  LOG_DEBUG("EVAL expr4: %d \n", eval_expression_node(expr4));

  // Multiply the whole tree by 2
  LOG_DEBUG("\n\n------- Multiply The Whole Thing By 2 ------ \n\n");
  node_t *expr3 = node_expression_from(&multiply_expression, '*');
  // node_t *multiplicand1 = node_value_from(1);
  node_t *multiplicand1 = node_value_from(3);
  expr3->child = multiplicand1;
  multiplicand1->sibling = expr1;
  LOG_DEBUG("EVAL NODE expr3: %d\n", eval_expression_node(expr3));
  //   LOG_DEBUG("\nEXPR 1: \n");
  //   bfs(expr1);
  //   LOG_DEBUG("\nEXPR 2: \n");
  //   bfs(expr2);
  //   LOG_DEBUG("\nEXPR 3: \n");
  //   bfs(expr3);
  //   LOG_DEBUG("\nEXPR 4: \n");
  //   bfs(expr4);
  //   LOG_DEBUG("\nDONE\n");
}
void debug_test_2() {
  char *test_str;
  node_t *test;
  int ret = 0;

  // LOG_DEBUG("Running test 2");
  // test_str = read_file("test/example_input/hw2-input02.txt");
  // test = parse_string(test_str);
  // LOG_DEBUG("Sent: %s\n", test_str);
  // ret = eval_expression_node(test);
  // LOG_DEBUG("eval: %d", ret);

  LOG_DEBUG("Running test 11");
  test_str = read_file("test/example_input/hw2-input11.txt");
  test = parse_string(test_str);
  LOG_DEBUG("Sent: %s\n", test_str);
  ret = eval_expression_node(test);
  printf("PID %d: Processed ", getpid());
  bfs(test);
  printf("; final answer is \"%d\"\n", ret);
  // printf("eval: %d", ret);
}
void debug_test_3() {
  // char expression1[] = "(- 18 (/ 14 3))";
  // node_t *test = parse_string(expression1);
  // LOG_DEBUG("Sent: %s\n", expression1);
  // int ret = eval_expression_node(test);
  // LOG_DEBUG("eval: %d", ret);
}
#endif

int main(int argc, char **argv) {
  tgc_start(&gc, &argc);
  if (!argv[1]) {
    printf("ERROR: Invalid arguments\n");
    printf("USAGE: %s <input-file>\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  char *test_str;
  node_t *test;
  int ret = 0;
  test_str = read_file(argv[1]);
  printf("PID %d: My expression is %s", getpid(), test_str);
  // bfs(test);
  // printf("\n");
  // LOG_DEBUG("Sent: %s\n", test_str);
  test = parse_string(test_str);
  // printf("PID %d: My expression is");
  // bfs(test);
  // printf("\n");
  // LOG_DEBUG("Sent: %s\n", test_str);
  ret = eval_expression_node(test);
  printf("PID %d: Processed \"", getpid());
  bfs(test);
  printf("\"; final answer is \"%d\"\n", ret);

#ifdef DEBUG
// debug_test_1();
// debug_test_2();
// debug_test_3();
#endif
  tgc_stop(&gc);
  return EXIT_SUCCESS;
}
