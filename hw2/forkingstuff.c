pid_t pid = -1;
if (rc == 0) { /* Child */
  // Take a recursive step and then put the value over the pipe.
  pid = getpid();
  LOG_DEBUG("CHILD process has pid %d", pid);
  int c_report = node->child->val;
  c_report = node->expr(report, eval_expression_node(next));
  write(fd[1],&c_report,sizeof(&c_report));
  LOG_DEBUG("CHILD %d : Sent %d ", pid, c_report);
  close(fd[1]);
  exit(EXIT_SUCCESS);
  LOG_DEBUG("ROUGE CHILD!");
} else { /* Parent */
  // Read the child value in over a pipe and then exit.
  printf("PARENT: my child process has pid %d\n", pid);
  int pid = getpid();
  printf("PARENT: my pid is %d\n", pid);
  read(fd[0],&report,sizeof(&report));
  LOG_DEBUG("PARENT %d : Received %d",pid, report );
  LOG_DEBUG("PARENT: waiting for my child process to terminate...\n");

  int status;
  pid_t child_pid = waitpid(-1, &status, 0);

  LOG_DEBUG("PARENT: child %d terminated...\n", child_pid);

  if (WIFSIGNALED(status)) { /* a signal (e.g., segfault, etc.)     */
    LOG_DEBUG("...abnormally\n");
  } else if (WIFEXITED(status)) /* child proc returned or called exit() */
  {
    int child_return_value = WEXITSTATUS(status);
    LOG_DEBUG("...successfully with exit status %d\n", child_return_value);
  }
