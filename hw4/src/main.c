/*
   Lucien Brule <brulel@rpi.edu>
   Rensselaer Polytechnic Institute
   CSCI 4210 Operating Systems HW4
 */

#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>
#include <stdlib.h>
#include <dirent.h>
#include <ctype.h>


#include "tgc.h"
#include "hw3.h"


bool is_file(const char *path) {
  struct stat path_stat;

  stat(path, &path_stat);

  if (S_ISREG(path_stat.st_mode) != 0) {
    return true;
  }
  return false;
}

bool is_directory(const char *path) {
  struct stat path_stat;

  stat(path, &path_stat);
  printf("---> isdir: %d\n", S_ISDIR(path_stat.st_mode));

  if (S_ISDIR(path_stat.st_mode) != 0) {
    return true;
  }
  return false;
}

bool valid_args(const int *argc, const char *argv[]) {
  #ifdef DEBUG_MODE
  printf("DEBUG: argc is %d\n", *argc);

  for (int i = 0; i < *argc; i++) {
    printf("DEBUG: argv[%d]: %s\n", i, argv[i]);
  }
  #endif /* ifdef DEBUG_MODE */

  if (*argc < 4) {
    return false;
  }

  if (!is_directory(argv[1])) {
    return false;
  }

  while (*argv[2]) {}

  return true;
}

static tgc_t gc;
int main(int argc, char const *argv[]) {
  tgc_start(&gc, &argc);

  if (!valid_args(&argc, argv)) {
    printf(
      "ERROR: Invalid arguments\nUSAGE: %s <input-directory> <buffer-size> <output-file>\n",
      argv[0]);
  }

  // Assume valid file path here
  DIR *d;
  struct dirent *dir;
  d = opendir("test/");

  if (d)
  {
    while ((dir = readdir(d)) != NULL)
    {
      printf("%s\n", dir->d_name);
    }

    closedir(d);
  }

  tgc_stop(&gc);
  return 0;
}
