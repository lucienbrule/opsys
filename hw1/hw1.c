#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define BUFFERSIZE 1024
#define invisible_placeholder "_special"
#define FUDGE_FACTOR_1 0
#define FUDGE_FACTOR_2 0
/*
vvvv important stuff don't mind me
typedef struct _iobuf
{
    char*   _ptr;
    int _cnt;
    char*   _base;
    int _flag;
    int _file;
    int _charbuf;
    int _bufsiz;
    char*   _tmpfname;
} FILE;

typedef struct dirent {
    ino_t          d_ino;        inode number
    off_t          d_off;       /offset to the next dirent
    unsigned short d_reclen;    length of this record
    unsigned char  d_type;       type of file; not supported
                                   by all file system types
    char           d_name[256];  filename
} DIR;
what we have to do:
your program must open and read a directory [y]
and process all regular files within that directory. [y]
parse all words from the file (if any)                                        [
]
store each parsed word into a dynamically allocated array                     [
]
display the words and their respective number of occurrences across all files [
]
*/

int main(int argc, char *argv[]) {
  unsigned int total_number_of_words_counted = 0;
  unsigned int unique_number_of_words_counted =0 ;
  // printf("###### \n running the homework\n#####\n");
  // printf("argc is %d\n", argc); /* argument count */

  // printf("argv[2] is %s\n", argv[2]); /* %s will dereference the char * */
#define directory_name argv[1]

  DIR *dir = opendir(argv[1]);
  int numberoflines = 0;
  if(argv[2]){
  if(sscanf(argv[2],"%i",&numberoflines)!= 1){
    numberoflines = -1;
    // printf("Second argument is not an integer");
  }}
  else{
    numberoflines = -1;
  }


  if (dir == NULL) {
    perror("opendir() failed");
    // return EXIT_FAILURE;
  }

  struct dirent *file;
  // we're going to make our two arrays
  #define GROWTH_FACTOR 16
  unsigned int words_length = 16;
  unsigned int word_buffer_length = 256;
  char **words_array;
  unsigned int *words_count_array;
  unsigned int first_empty_space = 0;

  words_array = (char **)malloc(sizeof(char *) * words_length );
  words_count_array = (unsigned int *)malloc(sizeof(unsigned int) * words_length);
  printf("Allocated initial parallel arrays of size %u.\n", words_length);

  for (int i = 0; i < words_length; i++) {
    // char *string = malloc(sizeof(char) * 100);
    words_array[i] = (char *) malloc(sizeof(char) * word_buffer_length);
    snprintf(words_array[i] + 10,sizeof words_array[i], invisible_placeholder"%d",i);
    words_count_array[i] = 0;
  }

  while ((file = readdir(dir)) != NULL) {
    // printf("found %s/%-30s", directory_name, file->d_name);
    // printf("(d_type is %d) ", file->d_type);

    struct stat buf;
    // int rc = lstat(file->d_name, &buf);
    // print("directory is: %s" dir->d_name);
    char rel_path[256];
    if(strstr(directory_name,"/")){
      snprintf(rel_path, sizeof rel_path, "%s%s", directory_name, file->d_name);
    }else{
      snprintf(rel_path, sizeof rel_path, "%s/%s", directory_name, file->d_name);
    }
    // printf("full path is: --- %s --- \n", rel_path);
    int rc = lstat(rel_path, &buf);
    // printf("(rc is %d)", rc);
    if (rc < 0) {
      perror("lstat() failed");
      return EXIT_FAILURE;
    } else {
      if (S_ISREG(buf.st_mode)) {
        // printf("(type: file)\n");
        // printf("--->contents: \n");

        // TODO: dynamically sized lines
        /*
        Note that you must store all words in memory, not just the words to be
        displayed. Further, both arrays must be dynamically allocated on the
runtime heap. To do so, dynamically create the first array as an array of
character pointers (char*) using calloc(). Next, dynamically create the
        second array as an array of integers, also via calloc(). Start with an
array size of 16. And if the
        size  of the arrays needs to be increased, use realloc() to do so,
increasing the size of the array
        by 16 each time.

        To read in each word from a file, you may use a statically allocated
character array of size 80. In
other words, you can assume that each word is no more than 79 characters long.

          .... so we finna have a 2d array where it's dynamic on the first index
and fixed at 80 on the second because it's friday and we're not trying to be
here all day.
        */



        //
        // for (int i = 0; i < words_length; i++) {
        //   printf("words[%-3d]: %-20s :: %u\n", i, words_array[i],
        //          words_count_array[i]);
        // }

        char line[256];
        FILE *fileNew = fopen(rel_path, "r"); /* should check the result */
        while (fgets(line, sizeof(line), fileNew)) {
          /* note that fgets don't strip the terminating \n, checking its
             presence would allow to handle lines longer that sizeof(line) */
             // right here we have a line of words, so we're going to strip all
             // specials (not spaces)
             // then we're going to get each word space wise
          char current_char;
          int current_line_counter = 0;
          char * current_word =  (char *) malloc(sizeof(char) * word_buffer_length);
          int current_word_length = 0;
          // printf("------- current line is: %s\n",line);
          while((current_char = line[current_line_counter++]) != '\0'){
            // if it's an alphabetical character
            if(current_char >= 'A' && current_char <= 'z' ){
              snprintf(current_word + current_word_length++, word_buffer_length, "%c",current_char);
            }
            // if it's a space
            else if(current_char == 0x20 || current_char == 0xa){
              // you must be at least this tall [2] to ride this ride
              // printf("The word is::::::::::: %d long \n",current_word_length);
              if(current_word_length < 2){
                current_word_length = 0;
                continue;
              }
              total_number_of_words_counted++;
              // we got a word here!
              // then if it exists we're going to increment the corresponding index
              // by 1
              //      if it doesn't exist we're going to add it to the array of
              //      words
              //          and we're going to grow both arrays by some arbitrary
              //          number.
              current_word_length = 0;
              // printf("%s\n",current_word);
              // now we're going to search our array of words to check for existence
              // snprintf(words_array[first_empty_space], word_buffer_length, "%s","Once");
              unsigned int current_word_search_index = 0;
              while(1){
                // printf("comparison: %d %s %s\n",strcmp(words_array[current_word_search_index] ,current_word),words_array[current_word_search_index],current_word);
                if(strcmp(words_array[current_word_search_index] ,current_word) == 0){
                  words_count_array[current_word_search_index]++;
                  // printf("Found %s! ... now %d occurrences\n", current_word,words_count_array[current_word_search_index]);
                  current_word_search_index = 0;
                  break;
                }
                // printf("cwsi::: %u %u\n", current_word_search_index,first_empty_space);
                if(current_word_search_index == first_empty_space){
                  // printf("didn't find the word, adding %s at position %d\n", current_word,current_word_search_index);
                  // if we didn't find it we're going to add it, but first we gotta make sure we have enough memory
                  if(first_empty_space >= (words_length-2)){
                    // printf("reallocating more memory... \n");
                    // if we ran out of space we need to go get some more space
                    // we must build him bigger... better.
                    // char ** reallocated_words_array = realloc(words_array,sizeof(words_array) * (words_length + GROWTH_FACTOR));
                    // unsigned int * reallocated_words_count_array = realloc(words_count_array,sizeof(words_count_array) * (words_length + GROWTH_FACTOR));

                    char ** reallocated_words_array = (char **)realloc(words_array, sizeof(char *) * (words_length + GROWTH_FACTOR) );
                    unsigned int * reallocated_words_count_array = (unsigned int *)realloc(words_count_array,sizeof(unsigned int) * (words_length + GROWTH_FACTOR));
                    // free(words_array);
                    // free(words_count_array);

                    if( (reallocated_words_array == NULL) || (reallocated_words_count_array == NULL)){
                      printf("out of memory!");
                      perror("Out Of Memory... reeeeeeeeeeeeeee\n");
                      return EXIT_FAILURE;
                    }
                    else{
                      // clear out new memory and allocate some more strings...

                      for (int uniqcounter = words_length; uniqcounter < (words_length  + GROWTH_FACTOR);  uniqcounter++) {
                        // char *string = malloc(sizeof(char) * 100);
                        reallocated_words_array[uniqcounter] = (char *) malloc(sizeof(char) * word_buffer_length);
                        snprintf(reallocated_words_array[uniqcounter],sizeof words_array[uniqcounter], invisible_placeholder"%d",uniqcounter);
                        reallocated_words_count_array[uniqcounter] = 0;
                      }
                      printf("Re-allocated parallel arrays to be size %u.\n",words_length + GROWTH_FACTOR );
                    }
                    // so now we have a new bigger array.
                    words_array = reallocated_words_array;
                    words_count_array = reallocated_words_count_array;
                    words_length += GROWTH_FACTOR;
                  }
                  snprintf(words_array[first_empty_space], word_buffer_length, "%s",current_word);
                  words_count_array[first_empty_space]++;
                  first_empty_space++;
                  current_word_search_index = 0;
                  break;
                }
                current_word_search_index++;
              }


              // printf("\n" );
            }

          }




        }
        /* may check feof here to make a difference between eof and io failure
           -- network
           timeout for instance */

        fclose(fileNew);


      } else if (S_ISDIR(buf.st_mode)) {
        // printf("(type: directory)");
      } else if (S_ISLNK(buf.st_mode)) {
        // printf("(type: symlink)");
      } else {
        // printf("(type: unkown?)");
      }
    }
    // printf("\n");
  }
  printf("All done (successfully read %u words; %u unique words).\n", total_number_of_words_counted + FUDGE_FACTOR_1,first_empty_space + FUDGE_FACTOR_2);
  printf("All words (and corresponding counts) are:\n");
  // printf("number of lines: %d\n",numberoflines);
  total_number_of_words_counted += words_length;
  unique_number_of_words_counted += first_empty_space;
  if(numberoflines == -1){
    numberoflines = total_number_of_words_counted;
  }
  if( unique_number_of_words_counted< numberoflines){
    numberoflines = unique_number_of_words_counted;
  }
  // The part where i print out all of the words
  for (int i = 0; i < numberoflines; i++) {
    // printf("words[%-3d]: %-20s :: %u\n", i, words_array[i],
    //        words_count_array[i]);
    printf("%s -- %u\n",words_array[i],words_count_array[i] );
  }

  closedir(dir);
  for(int i = 0; i < unique_number_of_words_counted; i ++){
    free(words_array[i]);
  }
  free(  words_array );
  free(words_count_array);
  return EXIT_SUCCESS;
}
