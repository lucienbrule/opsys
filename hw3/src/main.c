/*
   Lucien Brule <brulel@rpi.edu>
   Rensselaer Polytechnic Institute
   CSCI 4210 Operating Systems HW3
 */

#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "tgc.h"
#include "hw3.h"


int is_file(const char *path)
{
        struct stat path_stat;
        stat(path, &path_stat);
        return S_ISREG(path_stat.st_mode);
}
int is_directory(const char *path){
        struct stat path_stat;
        stat(path, &path_stat);
        return S_ISDIR(path_stat.st_mode);
}

bool valid_args(const int *argc, const char *argv[]){
  #ifdef DEBUG_MODE
        printf("DEBUG: argc is %d\n", *argc);
        for(int i = 0; i < *argc; i++) {
                printf("DEBUG: argv[%d]: %s\n",i, argv[i]);
        }
  #endif

        if(*argc < 4) {
                return false;
        }
        if(!is_directory(argv[0])) {
                return false;
        }


        return true;
}
static tgc_t gc;
int main(int argc, char const *argv[]) {
        tgc_start(&gc, &argc);
        if(!valid_args(&argc,argv)) {
                printf("ERROR: Invalid arguments\nUSAGE: ./a.out <input-directory> <buffer-size> <output-file>\n");
        }
        tgc_stop(&gc);
        return 0;
}
